<?php
require_once('model/include/s3.php'); //integrating aws s3
require_once('model/include/class.upload.php');
$s3 = new S3('Access Key','Secret Key');
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>S3</title>
    <style>
        ul{list-style: none;display: inline-flex;}
        ul li img {width:76%;}
        ul li a {display:inline;}
    </style>
</head>
<body>
<form action="controller/files.php" method="post" enctype="multipart/form-data">
    <table border="1">
        <tr>
            <td>Select Category</td>
            <td>
                <select name="category" id="">
                    <option value="photos">photos</option>
                    <option value="abstract">abstract</option>
                    <option value="illustrations">illustrations</option>
                    <option value="editorial">editorial</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Input Caption</td>
            <td><input type="text" name="caption"/></td>
        </tr>
        <tr>
            <td>Choose Your File</td>
            <td><input type="file" name="file"/></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="Upload >>"/>
            <?php echo @$_REQUEST['confirmation'];?>
            </td>
        </tr>
    </table>

</form>
<table border="1" width="50%" align="center">
    <tr>
        <td>Category</td>
        <td>File</td>
        <td>Download</td>
        <td>Delete</td>
    </tr>
<?php
// Get the contents of our bucket
$bucket_contents = $s3->getBucket("s3.urosd.com");
foreach ($bucket_contents as $file){
    $fname = $file['name'];
    $size = round($file['size']/1024,3);
    $furl = "https://s3.amazonaws.com/s3.urosd.com/".$fname;
    $category = explode('/',$fname);
    $category = $category[0];
    echo
    "<tr>
        <td>$category</td>
        <td>
            <img src='$furl' alt='No Preview' title='{$size}KB' width='50' height='50' />
        </td>
        <td><a href=\"$furl\" target='_blank'>click</a></td>
        <td><a href='controller/deleteObject.php?uploadName=$fname' target='_blank'>delete</a></td>
    </tr>";
}
?>
</table>
</body>
</html>